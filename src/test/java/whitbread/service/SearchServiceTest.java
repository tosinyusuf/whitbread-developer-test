package whitbread.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import whitbread.client.FourSquareApiClient;
import whitbread.model.Venue;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceTest {

  @Mock
  private FourSquareApiClient client;

  @InjectMocks
  private SearchService searchService;

  @Test
  public void venuesSearchReturnVenues() throws Exception {
    String searchTerm = "Victoria";

    when(client.venuesSearch(searchTerm)).thenReturn(buildVenueList());

    List<Venue> venues = searchService.venuesSearch(searchTerm);

    assertEquals(2, venues.size());
  }

  private List<Venue> buildVenueList() {
    Venue venue = new Venue();
    venue.setId("1");
    venue.setName("Victoria Coffee Shop");

    Venue venue2 = new Venue();
    venue2.setId("2");
    venue2.setName("Victoria Wine Bar");

    return Arrays.asList(venue, venue2);
  }
}