package whitbread.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import whitbread.WhitbreadDeveloperTestApplication;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WhitbreadDeveloperTestApplication.class)
@WebAppConfiguration
public class VenueSearchControllerTest {

  private MockMvc mockMvc;

  private MockRestServiceServer mockServer;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Autowired
  private RestTemplate restTemplate;

  @Before
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    this.mockServer = MockRestServiceServer.bindTo(this.restTemplate).ignoreExpectOrder(true).build();
  }

  @Test
  public void shouldPerformVenuesSearch() throws Exception {

    String responseBody = "[{\"id\":\"1\",\"name\":\"London Restaurant\"},{\"id\":\"2\",\"name\":\"London Coffee Shop\"}]";

    this.mockServer.expect(requestTo("https://api.foursquare.com/v2/venues/search?query=London")).andExpect(method(HttpMethod.GET))
      .andRespond(withSuccess(responseBody, MediaType.APPLICATION_JSON));

    mockMvc.perform(get("/venues/search?query=London"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$", hasSize(2)))
      .andExpect(jsonPath("$[0].id").value(equalTo("1")))
      .andExpect(jsonPath("$[0].name").value(equalTo("London Restaurant")));
  }
}