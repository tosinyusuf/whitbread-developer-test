package whitbread.client;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import whitbread.model.Venue;

import java.util.Arrays;
import java.util.List;

@Service
public class FourSquareApiClient {

  private static final String API_URL = "https://api.foursquare.com/v2/venues/search";

  private final RestTemplate restTemplate;

  public FourSquareApiClient(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public List<Venue> venuesSearch(String query) {
    Venue[] venues = restTemplate.getForObject(API_URL + "?query=" + query, Venue[].class);
    return Arrays.asList(venues);
  }
}
