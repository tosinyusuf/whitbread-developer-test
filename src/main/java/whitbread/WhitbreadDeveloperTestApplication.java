package whitbread;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import whitbread.config.AppConfiguration;

@SpringBootApplication
@Import(AppConfiguration.class)
public class WhitbreadDeveloperTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhitbreadDeveloperTestApplication.class, args);
	}
}
