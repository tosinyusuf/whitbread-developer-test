package whitbread.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import whitbread.client.FourSquareApiClient;
import whitbread.model.Venue;

import java.util.List;

@Service
public class SearchService {

  @Autowired
  private FourSquareApiClient client;

  public List<Venue> venuesSearch(String query) {
    return client.venuesSearch(query);
  }
}
