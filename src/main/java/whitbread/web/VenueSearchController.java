package whitbread.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import whitbread.model.Venue;
import whitbread.service.SearchService;

import java.util.List;

@RestController
@RequestMapping("/venues")
public class VenueSearchController {

  @Autowired
  private SearchService searchService;

  @RequestMapping(path="search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Venue> venuesSearch(@RequestParam(value="query") String query) {
    return searchService.venuesSearch(query);
  }

}
