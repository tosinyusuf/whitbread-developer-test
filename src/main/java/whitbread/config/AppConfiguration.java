package whitbread.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import whitbread.client.FourSquareApiClient;

@Configuration
public class AppConfiguration {

  @Autowired
  RestTemplateBuilder restTemplateBuilder;

  @Bean
  public FourSquareApiClient fourSquareApiClient() {
    return new FourSquareApiClient(restTemplate());
  }

  @Bean
  public RestTemplate restTemplate() {
    return restTemplateBuilder.build();
  }

}
