##Notes

The project demonstrates the Foursquare Api Integration for the Search Venues endpoint.
Note - I did not run this against the actual API as I wasn't keen on creating an account for FourSquare.
The intention is to show how I would go about integrating the API within a Spring boot application.
Hence running the application and accessing it via an example url:-
http://localhost:8080/venues/search?query=London

Results in a 500 response as the RestTemplate cannot access the real API.

VenueSearchControllerTest is an integration test which mocks the Foursquare Api.